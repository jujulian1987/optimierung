package  com.julianliebl.rubberexample.handlers;
import nme.display.Sprite;
import nme.display.DisplayObject;
import nme.events.Event;
import flash.Lib;
/**
 * ...
 * @author northlightgames - Julian Liebl
 */

class Display extends Sprite
{
	private var spriteLayer:Array<Sprite>;
	
	public function new() 
	{
		super();
		init();
	}
	
	public function init() {
		spriteLayer = new Array<Sprite>();
		for (i in 0...10) {
			spriteLayer.push(new Sprite());
			Lib.current.addChild(spriteLayer[i]);
		}
		
		//onResize
		Lib.current.stage.addEventListener(Event.RESIZE, onResize);
	}
	
	public function construct() {
		//nothing to do here
	}
	
	public function unload() {
		for (spriteItem in spriteLayer) {
			for (i in 0 ... spriteItem.numChildren) {
				spriteItem.removeChildAt(0);
			}
		}
	}
	
	public function onResize(e:Event) {
		unload();
		construct();
	}
	
	public function addToScene(dObject:DisplayObject, layer:Int) {
		if (layer < spriteLayer.length && layer >= 0) {
			spriteLayer[layer].addChild(dObject);
		}else {
			trace("Error: Layer not in out of bounds");
		}
	}
	
}