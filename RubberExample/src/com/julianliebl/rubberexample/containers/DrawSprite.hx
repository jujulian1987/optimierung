package com.julianliebl.rubberexample.containers;
import nme.display.Sprite;
import haxe.Timer;
/**
 * ...
 * @author Julian Liebl
 */

class DrawSprite extends Sprite
{
	public var timeStamp:Int;
	
	public function new() 
	{
		super();
		Timer.delay(removeMySelf, 1000);
	}
	
	public function removeMySelf() {
		this.parent.removeChild(this);
	}
}