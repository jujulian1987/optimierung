package  com.julianliebl.rubberexample.helpers;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
/**
 * ...
 * @author northlightgames
 */

class ImageHelper 
{

	public static var baseImagePath:String = "img/";
	public static var screenDensity:Float = 1;
	private static var cachedBitmapData:Hash <BitmapData> = new Hash <BitmapData> ();

	public static function loadBitmap (path:String, cache:Bool = true):Bitmap {
		var bitmap:Bitmap;
		
		if (cache) {
			if (!cachedBitmapData.exists (path)) {
				cachedBitmapData.set (path, Assets.getBitmapData (baseImagePath + path));
			}
			bitmap = new Bitmap (cachedBitmapData.get (path));
		} else {
			bitmap = new Bitmap (Assets.getBitmapData (baseImagePath + path));
		}
		
		return bitmap;
	}

	public static function loadSprite (path:String, cache:Bool = true):Sprite {
		var bitmap = loadBitmap (path, cache);
		var sprite = new Sprite ();
		sprite.addChild (bitmap);

		#if js
		
		var hitArea:Sprite = new Sprite ();
		hitArea.graphics.beginFill (0xFFFFFF);
		hitArea.graphics.drawRect (0, 0, bitmap.width, bitmap.height);
		hitArea.alpha = 0;
		sprite.addChild (hitArea);
		
		#end
		return sprite;
	}
	
}