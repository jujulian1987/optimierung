package com.julianliebl.rubberexample;
import  com.julianliebl.rubberexample.handlers.Display;
import  com.julianliebl.rubberexample.helpers.ImageHelper;
import com.julianliebl.rubberexample.containers.DrawSprite;

import nme.display.Sprite;
import nme.events.MouseEvent;
import nme.events.Event;
import nme.geom.Point;
import nme.Lib;

/**
 * ...
 * @author Julian Liebl
 */

class Scene extends Display
{
	private var sprite1:Sprite;
	private var sprite2:Sprite;
	private var spriteMask:Sprite;
	private var spriteBrush:Sprite;
	
	private var mouseDown:Bool;
	
	//vars for fixed update
	private var oldTime:Int;
	private var oldCord:Point;
	
	public function new() 
	{
		super();
	}
	
	public override function init() {
		super.init();
		
		oldTime = Lib.getTimer();
		oldCord = new Point(9999, 9999);
		
		spriteMask = new Sprite(); 
		sprite1 = ImageHelper.loadSprite("1.jpg",true);
		sprite2 = ImageHelper.loadSprite("2.jpg", true);
		
		spriteMask.cacheAsBitmap = true;		
		sprite2.cacheAsBitmap = true;
		
		sprite2.mask = spriteMask;
		
		addToScene(sprite1, 1);
		addToScene(sprite2, 2);
		addToScene(spriteMask, 2);
		
		Lib.current.addEventListener(MouseEvent.MOUSE_UP, updateMouseStatus);
		Lib.current.addEventListener(MouseEvent.MOUSE_DOWN, updateMouseStatus);
		Lib.current.addEventListener(Event.ENTER_FRAME, update);
	}
	
	public function update(e:Event) {
		var timePassed = Lib.getTimer() - oldTime;
		
		if (timePassed > 3) {
			oldTime = Lib.getTimer();
			fixedUpdate();
		}
	}
	
	public function fixedUpdate() {
		if (mouseDown) {
			if (oldCord.equals(new Point(9999, 9999))) {
				oldCord.x = sprite2.mouseX;
				oldCord.y = sprite2.mouseY;
			}else {
				var newCord:Point = new Point(sprite2.mouseX, sprite2.mouseY);
				if (newCord != oldCord) {
					var drawSprite:DrawSprite = new DrawSprite();
					drawSprite.addChild(ImageHelper.loadSprite("brush_01.png", true));
					
					drawSprite.x = newCord.x-drawSprite.width/2;
					drawSprite.y = newCord.y-drawSprite.height/2;
					
					spriteMask.addChild(drawSprite);
					
					oldCord = newCord;
				}
			}
		}
	}
	
	public function updateMouseStatus(e:MouseEvent) {
		mouseDown = e.buttonDown;
		if (!mouseDown) {
			oldCord = new Point(9999, 9999);
		}
	}
	
}